package dd.sendspace;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Textbox;

import sendspaceapi.Data.AnonymousFileEntry;
import sendspaceapi.Upload.Anonymous.AnonymousUpload;

public class SendSpaceWindowComposer extends GenericForwardComposer {

	/**
	 * SendSpace ZK Uploader Send and go home :)
	 * 
	 * @author DiogoDuarte
	 */
	private static final long serialVersionUID = -4533393095539134635L;
	// put you API key here

	private Menuitem DDUploader;
	private Textbox txEmail;
	private Textbox txServer;
	private Textbox txPort;
	private Textbox txKey;
	
	/**
	 * Initialize
	 * 
	 */
	public SendSpaceWindowComposer() {
	}
	/**
	 * AfterCompose
	 * 
	 */
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		// Read properties file.
		loadProperties();

	}
	/**
	 * On Blur
	 * 
	 */
	public void onBlur$txEmail(Event event) {
		if (txEmail.isValid()) {
			DDUploader.setDisabled(false);
		} else {
			DDUploader.setDisabled(true);
		}
	}
	
	/**
	 * Thread that forks the process to avoid waiting.
	 * 
	 */
	public class StartUpload extends Thread {
		String FileName1;

		public void FileName(String name) {
			FileName1 = name;
			start();
		}

		public void run() {
			String[] args1;
			args1 = new String[1];
			args1[0] = FileName1;
			send(args1);
			File f = new File(FileName1);
			f.delete();
		}
	}
	
	/**
	 * Fires when the user selects file for upload big files to upload to
	 * sendspace
	 * 
	 */
	public void onUpload$DDUploader(UploadEvent event) {
		Media media = event.getMedia();
		String f1 = saveFile(media);
		StartUpload Su1 = new StartUpload();
		Su1.FileName(f1);
	}

	/**
	 * Sends email to user
	 * 
	 */
	public static void sendEmailToUser(AnonymousFileEntry e, String toMail) {
		String from = "SENDSPACE@gmail.com";
		String to = toMail;
		String subject = "FICHEIRO ENVIADO PARA SENDSPACE.COM";
		String message = "\n"
				+ "Nome do ficheiro: "
				+ e.fileName()
				+ "\n\n"
				+ "Links: \n"
				+ "Link para Download: "
				+ e.downloadURL()
				+ "\n"
				+ "\n"
				+ "Link para Apagar: "
				+ e.deleteURL()
				+ "\n\n"
				+ "Basta enviar o link para download.\n O link para apagar deve ser retirado!";

		SendMail sendMail = new SendMail(from, to, subject, message);
		sendMail.send();
	}

	/**
	 * Saves the file locally on the server.
	 * 
	 */
	@SuppressWarnings("finally")
	private String saveFile(Media media) {
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		String SAVE_PATH = Sessions.getCurrent().getWebApp().getRealPath(
				"/files")
				+ "\\";
		try {
			InputStream fin = media.getStreamData();
			in = new BufferedInputStream(fin);
			File baseDir = new File(SAVE_PATH);

			if (!baseDir.exists()) {
				baseDir.mkdirs();
			}

			File file = new File(SAVE_PATH + media.getName());

			OutputStream fout = new FileOutputStream(file);
			out = new BufferedOutputStream(fout);
			byte buffer[] = new byte[1024];
			int ch = in.read(buffer);
			while (ch != -1) {
				out.write(buffer, 0, ch);
				ch = in.read(buffer);
			}
			System.out.println("sucessed upload :" + media.getName());
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			try {
				if (out != null)
					out.close();

				if (in != null)
					in.close();
				return SAVE_PATH + media.getName();

			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Send the file
	 * 
	 */
	public void send(String[] args) {
		try {
			AnonymousUpload au = new AnonymousUpload(txKey.getValue());
			System.out.println("uploading file(s)...");
			if (args.length <= 0) {
				System.out.println("No files to upload!");
				return;
			} else if (args.length == 1) {
				AnonymousFileEntry e = au.uploadFile(new File(args[0]));
				sendEmailToUser(e, txEmail.getValue());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static File[] createFileList(String[] files) {
		File[] f = new File[files.length];
		for (int i = 0; i < files.length; i++) {
			f[i] = new File(files[i]);
		}
		return f;
	}
	
	public void onClick$btSave(Event event) {
		saveProperties();
	}
	
	public void loadProperties() {
		Properties pro = new Properties();

		try {
			String SAVE_PATH = Sessions.getCurrent().getWebApp().getRealPath(
					"/files")
					+ "\\";
			FileInputStream  f = new FileInputStream (SAVE_PATH + "conf.properties");
			pro.load(f);
			// load the props
			txServer.setValue(pro.getProperty("smtpserver"));
			txPort.setValue(pro.getProperty("smtpport"));		
			txKey.setValue(pro.getProperty("key"));		
			f.close();

		} catch (IOException e) {
		}

	}
	
	public void saveProperties() {
		Properties pro = new Properties();

		try {
			String SAVE_PATH = Sessions.getCurrent().getWebApp().getRealPath(
					"/files")
					+ "\\";
			FileOutputStream f = new FileOutputStream(SAVE_PATH
					+ "conf.properties");
			pro.setProperty("smtpserver", txServer.getValue());
			pro.setProperty("smtpport", txPort.getValue());
			pro.setProperty("key", txKey.getValue());
			pro.store(f, null);
			f.close();

		} catch (IOException e) {
		}

	}
}
